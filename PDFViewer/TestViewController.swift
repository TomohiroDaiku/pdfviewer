//
//  ReportDetail3ViewController.swift
//  ZaitakuReport
//
//  Created by 大工智博 on 2015/09/18.
//  Copyright (c) 2015年 medikaruno. All rights reserved.
//

import UIKit

class TestViewController: UIViewController, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var img_Freehand: UIImageView!
    @IBOutlet weak var img_background: UIImageView!
    var lineColor:UIColor = UIColor.blackColor()
    var lineWidth:CGFloat = 3.0
    var lastDrawImage: UIImage!
    var bezierPath: UIBezierPath!
    var undoStack: NSMutableArray!
    var redoStack: NSMutableArray!
    override func viewDidLoad() {
        super.viewDidLoad()
        //ローカルに保存してある画像にアクセスする
        if let image = UIImage(named: "test"){
            img_background.image = image
        }
        
    }
    /**
    * View初期表示時の処理
    */
    override func viewWillAppear(animated: Bool) {
        undoStack = NSMutableArray()
        redoStack = NSMutableArray()
    }
    /**
    * タッチ開始時の処理
    */
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first{
            let currentPoint:CGPoint = touch.locationInView(img_Freehand)
            bezierPath = UIBezierPath()
            bezierPath.lineCapStyle = CGLineCap.Round
            bezierPath.lineWidth = lineWidth
            bezierPath.moveToPoint(currentPoint)
        }
    }
    
    /**
    * タッチ移動時の処理
    */
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if bezierPath == nil {
            return
        }
        if let touch = touches.first{
            let currentPoint:CGPoint = touch.locationInView(img_Freehand)
            bezierPath.addLineToPoint(currentPoint)
            drawLine(bezierPath)
        }
    }
    
    /**
    * タッチ終了時の処理
    */
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        if bezierPath == nil {
            return
        }
        //let touch = touches.first as! UITouch
        //let currentPoint:CGPoint = touch.locationInView(img_Freehand)
        //bezierPath.addLineToPoint(currentPoint)
        //drawLine(bezierPath)
        lastDrawImage = img_Freehand.image
        undoStack.addObject(bezierPath)
        redoStack.removeAllObjects()
    }
    
    /**
    * 描画処理
    */
    func drawLine(path:UIBezierPath) {
        UIGraphicsBeginImageContext(img_Freehand.frame.size)
        if lastDrawImage != nil {
            lastDrawImage.drawAtPoint(CGPointZero)
        }
        let blackColor = lineColor
        blackColor.setStroke()
        path.stroke()
        img_Freehand.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    @IBAction func btn_ClearClick(sender: UIButton) {
        lastDrawImage = nil
        img_Freehand.image = nil
        undoStack = NSMutableArray()
        redoStack = NSMutableArray()
    }
    
    @IBAction func btn_ChangeModeClick(sender: UIButton) {
        lineColor = UIColor(red: 255, green: 255, blue: 0, alpha: 0.3)
        lineWidth = 20.0
    }
    @IBAction func btn_UndoClick(sender: UIButton) {
        if(undoStack == nil || undoStack.count == 0){return}
        let undoPath: AnyObject? = undoStack.lastObject
        undoStack.removeLastObject()
        redoStack.addObject(undoPath!)
        
        lastDrawImage = nil
        img_Freehand.image = nil
        
        for path in undoStack {
            if path is NSString {
                lastDrawImage = nil
            } else {
                drawLine(path as! UIBezierPath)
                lastDrawImage = img_Freehand.image
            }
        }
    }
}
