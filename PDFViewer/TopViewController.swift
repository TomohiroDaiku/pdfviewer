//
//  ViewController.swift
//  PDFViewer
//
//  Created by 大工智博 on 2015/10/21.
//  Copyright © 2015年 medikaruno. All rights reserved.
//

import UIKit
import M13PDFKit
import ROThumbnailGenerator

class TopViewController: UIViewController,DBRestClientDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate  {
    
    let HOME_PATH = "/アプリ/PDFViewer_KatahiraHospital/"
    var dbMetadatas:[DBMetadata]?
    var searchDBMetadatas:[DBMetadata] = []
    var dbRestClient: DBRestClient!
    @IBOutlet weak var txt_Search: UITextField!
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    let path:NSString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
    
    func initDropboxRestClient() {
        dbRestClient = DBRestClient(session: DBSession.sharedSession())
        dbRestClient.delegate = self
    }
    
    @IBOutlet weak var collection_DropBoxList: UICollectionView!
    
    @IBOutlet weak var txt_Path: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "片平ひふ科"
        
        self.tapGesture.cancelsTouchesInView = false
        
        //Dropboxへの接続
        if !DBSession.sharedSession().isLinked(){
            DBSession.sharedSession().linkFromController(self)
        }
        initDropboxRestClient()
        
        collection_DropBoxList.delegate = self
        collection_DropBoxList.dataSource = self
        txt_Search.delegate = self
        
        
        txt_Path.text = HOME_PATH
    }

    override func viewDidAppear(animated: Bool) {
        btn_ListClick(UIButton())
    }
    
    func restClient(client: DBRestClient, loadedMetadata metadata: DBMetadata) {
        if metadata.isDirectory {
            NSLog("Folder '%@' contains:", metadata.path)
            dbMetadatas = metadata.contents as? [DBMetadata]
            for file: DBMetadata in metadata.contents as! [DBMetadata] {
                NSLog("    %@", file.filename)
                print(file.thumbnailExists)
                print("サイズ" + (file.totalBytes/1024).description + "KB")
            }
        }
        collection_DropBoxList.reloadData()
        CustomProgress.Instance.mrprogress.dismiss(true)
    }
    
    func getFormatTotalByte(totleByte:Int64) -> String{
        if(totleByte < 1024){
            return totleByte.description + "B"
        }
        
        var formatTotalByte:Double = floor(Double(totleByte) / 1024.0)
        if(formatTotalByte < 1024){
            return Int(formatTotalByte).description + "KB"
        }
        
        formatTotalByte = Double(formatTotalByte) / 1024.0
        formatTotalByte = round(formatTotalByte * 10) / 10.0
        if(formatTotalByte < 1024){
            return formatTotalByte.description + "MB"
        }
        
        formatTotalByte = Double(formatTotalByte) / 1024.0
        formatTotalByte = Double(round(formatTotalByte * 10)) / 10.0
        return formatTotalByte.description + "GB"
    }
    
    func restClient(client: DBRestClient!, uploadFileFailedWithError error: NSError!) {
        print("File upload failed.")
        print(error.description)
    }
    
    @IBAction func btn_ListClick(sender: UIButton) {
        let initVal = CustomProgressModel()
        CustomProgress.Create(self.view, initVal: initVal, modeView:EnumModeView.MRActivityIndicatorView)
        CustomProgress.Instance.mrprogress.titleLabelText = "一覧取得中"
        self.dbRestClient.loadMetadata(txt_Path.text)
    }
    
    @IBAction func btn_HomeClick(sender: UIButton) {
        txt_Path.text = HOME_PATH
        btn_ListClick(UIButton())
    }
    
    /*
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let viewer: PDFKBasicPDFViewer = segue.destinationViewController as! PDFKBasicPDFViewer
        
        let _path = path.stringByAppendingPathComponent("test.pdf")
        
        let document = PDFKDocument(contentsOfFile: _path, password: nil)
        viewer.loadDocument(document)
    }*/
    
    /*
    Cellが選択された際に呼び出される
    */
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        //フォルダの場合は一覧を変更
        if(dbMetadatas![indexPath.row].isDirectory){
            txt_Path.text = dbMetadatas![indexPath.row].path
            btn_ListClick(UIButton())
            return
        }
        
        //未対応拡張子対応
        if (dbMetadatas![indexPath.row].filename as NSString).pathExtension != "pdf"{
                Alert.showSimpleAlert("エラー", message: "ver0.1\nPDF以外の表示には対応してません。", defaultTitle: "OK", view: self)
                    return;
        }
        
        //ローカルにファイルがある場合
        let downloadPath = path.description + "/" + dbMetadatas![indexPath.row].filename
        if(NSFileManager().fileExistsAtPath(downloadPath)){
            movePDFViewer(downloadPath,dbMetadatas: dbMetadatas![indexPath.row])
            return
        }
        
        let initVal = CustomProgressModel()
        CustomProgress.Create(self.view, initVal: initVal, modeView:EnumModeView.MRActivityIndicatorView)
        CustomProgress.Instance.mrprogress.titleLabelText = "ダウンロード中"
        let selectedFile: DBMetadata = dbMetadatas![indexPath.row]
        dbRestClient.loadFile(selectedFile.path, intoPath: downloadPath as String)
    }
    
    func movePDFViewer(downloadPath:String,dbMetadatas:DBMetadata){
        // 遷移するViewを定義する.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewer:PDFKBasicPDFViewer = storyboard.instantiateViewControllerWithIdentifier("PDFViewer") as! PDFKBasicPDFViewer
        // アニメーションを設定する.
        viewer.modalTransitionStyle = UIModalTransitionStyle.PartialCurl
        //let _path = path.stringByAppendingPathComponent("test.pdf")
        
        let document = PDFKDocument(contentsOfFile: downloadPath, password: nil)
        viewer.loadDocument(document)
        viewer.title = dbMetadatas.filename
        self.navigationController?.pushViewController(viewer, animated: true)
        
    }
    
    func restClient(client: DBRestClient!, loadedFile destPath: String!, contentType: String!, metadata: DBMetadata!) {
        //サムネイルの保存
        let url = NSURL(fileURLWithPath: destPath)
        let thumbnailImage = ROThumbnail.sharedInstance.getThumbnail(url)
        let pngImg = UIImagePNGRepresentation(thumbnailImage)
        pngImg!.writeToFile(destPath + "_thumb.png", atomically: true)
        
        movePDFViewer(destPath,dbMetadatas: metadata)
        CustomProgress.Instance.mrprogress.dismiss(true)
    }
    
    @IBAction func tap_ViewTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    /*
    @IBAction func txt_SearchValueChanged(sender: AnyObject) {
        setSearchMetadatas()
        self.collection_DropBoxList.reloadData()
    }
    
    @IBAction func txt_SearchTouchCancel(sender: UITextField) {
        setSearchMetadatas()
        self.collection_DropBoxList.reloadData()
    }
    @IBAction func txt_SearchEditingDidEnd(sender: UITextField) {
        setSearchMetadatas()
        self.collection_DropBoxList.reloadData()
    }
    */
    func textFieldShouldEndEditing(textField: UITextField) -> Bool{
        setSearchMetadatas()
        self.collection_DropBoxList.reloadData()
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
    
    func setSearchMetadatas(){
        searchDBMetadatas = []
        if(txt_Search.text == nil || txt_Search.text == ""){
            return
        }else{
            for metaData in dbMetadatas!{
                if(metaData.filename.containsString(txt_Search.text!)){
                    searchDBMetadatas.append(metaData)
                }
            }
            return
        }
    }
    
    /*
    Cellの総数を返す
    */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(dbMetadatas != nil){
            if(txt_Search.text == nil || txt_Search.text == ""){
                return dbMetadatas!.count
            }else{
                return searchDBMetadatas.count
            }
        }else{
            return 0
        }
    }
    
    /*
    Cellに値を設定する
    */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let setMetadatas:[DBMetadata]?
        
        if(txt_Search.text == nil || txt_Search.text == ""){
            setMetadatas = dbMetadatas
        }else{
            setMetadatas = searchDBMetadatas
        }
        
        
        let cell : CustomUICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("CustomCell",
            forIndexPath: indexPath) as! CustomUICollectionViewCell
        
        //cell.backgroundColor = UIColor.lightGrayColor()
        cell.layer.cornerRadius = 10
        cell.layer.borderColor = UIColor.lightGrayColor().CGColor
        cell.layer.borderWidth = 1
        let title = (setMetadatas![indexPath.row].filename as NSString).lastPathComponent
        cell.lbl_Title.text = title
        cell.img_Thum.image = nil
        if(setMetadatas![indexPath.row].isDirectory){
            cell.img_Type.image = UIImage(named: "icon_folder")
            cell.lbl_Size.text = ""
        }else{
            let pathExtension = (setMetadatas![indexPath.row].filename as NSString).pathExtension
            cell.lbl_Size.text = getFormatTotalByte(setMetadatas![indexPath.row].totalBytes)
            if(pathExtension == "pdf"){
                cell.img_Type.image = UIImage(named: "icon_pdf")
                
                let thumbPath = path.description + "/" + cell.lbl_Title.text! + "_thumb.png"
                //ローカルに保存してある画像にアクセスする
                if let image = UIImage(contentsOfFile: thumbPath){
                    cell.img_Thum.image = image
                }
                /*
                if(NSFileManager().fileExistsAtPath(thumbPath)){
                    let url = NSURL(fileURLWithPath: thumbPath)
                    //let thumbnailImage = ROThumbnail.sharedInstance.getThumbnail(url)
                    //cell.img_Thum.image = thumbnailImage
                    /*
                    //let url = NSURL(fileURLWithPath: downloadPath)
                    let document = PDFKDocument(contentsOfFile: downloadPath, password: nil)
                    //Load the thumb
                    //let request:PDFKThumbRequest = PDFKThumbRequest.newForView(PDFKThumbView(), fileURL: url, password: "", guid: "", page: 1, size: CGSizeMake(50, 60))
                    let request:PDFKThumbRequest = PDFKThumbRequest.newForView(PDFKBasicPDFViewerThumbsCollectionViewCell().thumbView, fileURL: document.fileURL, password: document.password, guid: document.guid, page: 1, size: CGSizeMake(50, 60))
                    //(cell.thumbView fileURL:_document.fileURL password:_document.password guid:_document.guid page:pageToDisplay size:[self collectionView:self layout:self.collectionViewLayout sizeForItemAtIndexPath:indexPath]];
                    let image:UIImage = PDFKThumbCache.sharedCache().thumbRequest(request, priority: true) as! UIImage
                    //thumbRequest:request priority:YES];
                    cell.img_Thum.image = image
                    */
                }else{
                    
                }
                */
                
            }else if(pathExtension == "png"){
                cell.img_Type.image = UIImage(named: "icon_png")
                cell.lbl_Size.text = ""
            }else{
                cell.img_Thum.image = nil
            }
        }
        return cell
    }
}

class CustomUICollectionViewCell : UICollectionViewCell{
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Thum: UIImageView!
    @IBOutlet weak var lbl_Size: UILabel!
    @IBOutlet weak var img_Type: UIImageView!
}

