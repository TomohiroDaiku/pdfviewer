//
//  Progress.swift
//  HomeApp
//
//  Created by 大工智博 on 2015/04/10.
//  Copyright (c) 2015年 marunoPharmacy. All rights reserved.
//

import MRProgress

enum EnumModeView{

    case MRActivityIndicatorView
    case MRCircularProgressView
    case UIProgressView
    case UIActivityIndicatorView
    case MRCheckmarkIconView
    case MRCrossIconView
    case None
}

/*
初期値オブジェクト
*/
class CustomProgressModel{
    init(){}
    init(title:String){
        self.title = title
    }
    var progress:Float = 0.0
    var title:String = "Loading..."
    var trackTintColor:UIColor = UIColor.whiteColor()
    var progressTintColor:UIColor = UIColor.blueColor()
    //** tdaiku add property
    var compliteTitle:String = "Complite!!"
}

class CustomProgress{
    //static init 用
    init(){
        Instance.progressChange = 0
        Instance.modeView = EnumModeView.None
        Instance.view = nil
    }
    
    //static struct
    struct Instance{
        static var mrprogress:MRProgressOverlayView = MRProgressOverlayView()
        static var modeView = EnumModeView.None
        static var view:UIView! = nil
        static var progressChange:Float = 0.0{
            didSet {
                if(CustomProgress.Instance.progressChange >= 1.0){
                    progressChange = 1.0
                    CustomProgress.Instance.mrprogress.titleLabelText = compliteTitle
                }
                mrprogress.setProgress(progressChange, animated: true)
            }
        }
        static var compliteTitle = "complite!!"
    }
    
    class func Create(view:UIView,initVal:CustomProgressModel,modeView:EnumModeView){
        
        Instance.mrprogress = MRProgressOverlayView()
        Instance.modeView = modeView
        Instance.view = nil
        Instance.compliteTitle = initVal.compliteTitle
        
        let mrprogress = Instance.mrprogress
        mrprogress.titleLabelText = initVal.title
        
        Application.isShowProgress = true
        
        switch modeView{
            
        case .UIProgressView:
            mrprogress.mode = MRProgressOverlayViewMode.DeterminateHorizontalBar
            
            let progress = UIProgressView()
            progress.progressViewStyle = UIProgressViewStyle.Default
            progress.progress = initVal.progress
            progress.trackTintColor = initVal.trackTintColor
            progress.progressTintColor = initVal.progressTintColor
            mrprogress.modeView = progress
            
        case .UIActivityIndicatorView:
            mrprogress.mode = MRProgressOverlayViewMode.IndeterminateSmall
            let progress = UIActivityIndicatorView()
            progress.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            mrprogress.modeView = progress
            
        case .MRCrossIconView:
            MRProgressOverlayView.showOverlayAddedTo(view,title:initVal.title,mode:MRProgressOverlayViewMode.Cross, animated: true);
            Instance.view = view
            return
            
        case .MRCheckmarkIconView:
            MRProgressOverlayView.showOverlayAddedTo(view,title:initVal.title,mode:MRProgressOverlayViewMode.Checkmark, animated: true);
            Instance.view = view
            return
            
        case .MRCircularProgressView:
            mrprogress.mode = MRProgressOverlayViewMode.DeterminateCircular
            mrprogress.setTintColor(initVal.progressTintColor)
            let progress = MRCircularProgressView()
            progress.animationDuration = 0.3
            //progress.mayStop = true
            progress.progress = initVal.progress
            mrprogress.modeView = progress
            break;

        case .MRActivityIndicatorView:
            break
            
        case .None:
            break
        }
        
        Instance.view = view
        view.addSubview(mrprogress)
        mrprogress.show(true)
        
    }
    
    class func isEnabledProgress() -> Bool{
        if CustomProgress.Instance.modeView == EnumModeView.UIProgressView ||
           CustomProgress.Instance.modeView == EnumModeView.MRCircularProgressView{
            return true
        }else{
            return false
        }
    }
    
    class func showError(title:String = "ネットワークに\n接続出来ません"){
        if Application.currentView != nil{
            let initVal = CustomProgressModel(title: title)
            self.Create(Application.currentView!, initVal:initVal, modeView: EnumModeView.MRCrossIconView)
        }
    }
    
    class func destroy(){
        MRProgressOverlayView.dismissAllOverlaysForView(CustomProgress.Instance.view, animated: true)
        if CustomProgress.Instance.view != nil{
            Application.isShowProgress = false
            //CustomProgress.Instance.view = nil
        }
        //CustomProgress.Instance.view = nil
    }
}

/*
progress完了時にtouchBeginイベントが動作したらprogressを削除する
*/
extension MRProgressOverlayView{
    var complite:Bool{
        get{
            if(CustomProgress.isEnabledProgress()){
                if(CustomProgress.Instance.progressChange >= 1.0){
                    CustomProgress()
                    return true
                }else{
                    return false
                }
            }
            return true
        }
    }
    
    override public func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        switch CustomProgress.Instance.modeView{
        case .None,.UIActivityIndicatorView:
            break;
        case .MRCheckmarkIconView,.MRCrossIconView:
            /*
            CustomProgress.Instance.mrprogress.dismiss(true)
            CustomProgress.Instance.view = nil
*/
            CustomProgress.destroy()
        case .UIProgressView,.MRCircularProgressView:
            if(CustomProgress.Instance.mrprogress.complite){
                CustomProgress.destroy()
            }
        default:
            CustomProgress.destroy()
        }
        
    }
}
