//
//  Alert.swift
//  HomeApp
//
//  Created by 大工智博 on 2015/04/03.
//  Copyright (c) 2015年 marunoPharmacy. All rights reserved.
//

//import Foundation
import UIKit

protocol AlertDelegate{
    func alertClose(title:String)
}

/*

利用時
1．AlertDelegateプロトコルを適合する。
2．title,messageを設定してdefaultActionArrayに設定したいラベル(String)をappendする。
3．preferredStyleにAlertかActionSheetが設定可能
4．isHaveCancelでキャンセルが必要か設定可能
5．showAlertControllerをCallする

TODO
・メンバ変数はprivateにすべき
・isHaveCancelって名前おかしくないか？
・もっとスマートにかけないか
*/
class Alert{
    
    //イニシャライザ
    convenience init(){
        self.init(title: "default",message: "default")
    }
    
    convenience init(title:String,message:String){
        self.init(title: title,message: message,preferredStyle: UIAlertControllerStyle.Alert)
    }
    
    init(title:String,message:String,preferredStyle:UIAlertControllerStyle){
        self.title = title
        self.message = message
        self.preferredStyle = preferredStyle
    }
    
    var delegate:AlertDelegate!
    
    //Alert表示設定項目
    var isHaveCancel:Bool = true;
    var title:String = "";
    var message:String = "";
    var preferredStyle:UIAlertControllerStyle = UIAlertControllerStyle.Alert
    
    var defaultActionArray:[String] = Array()
    
    /*
    シンプルなアラート(タイトル、メッセージ、確認ボタン)
    を表示する
    */
    class func showSimpleAlert(title:String,message:String,defaultTitle:String = "はい",view:UIViewController){
        let alert:UIAlertController = UIAlertController(title:title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        //defaultアクションの追加
        let defaultAction:UIAlertAction = UIAlertAction(title: defaultTitle,
            style: UIAlertActionStyle.Default,
            handler:nil)
        alert.addAction(defaultAction)
        view.presentViewController(alert, animated: true, completion: nil)
    }
    
    /*
    シンプルなアラート(タイトル、メッセージ、確認ボタン)
    を表示する
    */
    class func showSimpleAlert(title:String,message:String,defaultTitle:String = "はい",view:UIViewController,handler: ((UIAlertAction!) -> Void)!){
        let alert:UIAlertController = UIAlertController(title:title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        //defaultアクションの追加
        let defaultAction:UIAlertAction = UIAlertAction(title: defaultTitle,
            style: UIAlertActionStyle.Default,
            handler:handler)
        alert.addAction(defaultAction)
        view.presentViewController(alert, animated: true, completion: nil)
    }
    
    /*
    シンプルな質問(タイトル、メッセージ、YES NO)
    を表示する
    YESの時のみhandlerの処理を実行する
    */
    class func showQuestionAlert(title:String,message:String,view:UIViewController,handler: ((UIAlertAction!) -> Void)!){
        let alert:UIAlertController = UIAlertController(title:title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction:UIAlertAction = UIAlertAction(title: "いいえ",
            style: UIAlertActionStyle.Cancel,
            handler:{
                (action:UIAlertAction) -> Void in
        })
        
        alert.addAction(cancelAction)
        
        //defaultアクションの追加
        let defaultAction:UIAlertAction = UIAlertAction(title: "はい",
            style: UIAlertActionStyle.Default,
            handler:handler)
        alert.addAction(defaultAction)
        
        view.presentViewController(alert, animated: true, completion: nil)
    }

    /*
    シンプルな質問(タイトル、メッセージ、YES NO)
    を表示する
    */
    class func showQuestionAlert(title:String,message:String,view:UIViewController,yesHandler: ((UIAlertAction!) -> Void)!,noHandler: ((UIAlertAction!) -> Void)!){
        let alert:UIAlertController = UIAlertController(title:title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        let cancelAction:UIAlertAction = UIAlertAction(title: "いいえ",
            style: UIAlertActionStyle.Cancel,
            handler:noHandler)
        
        alert.addAction(cancelAction)
        
        //defaultアクションの追加
        let defaultAction:UIAlertAction = UIAlertAction(title: "はい",
            style: UIAlertActionStyle.Default,
            handler:yesHandler)
        alert.addAction(defaultAction)
        
        view.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showAlertController(view:UIViewController){
        let alert:UIAlertController = UIAlertController(title:title,
            message: message,
            preferredStyle: preferredStyle)
        
        //キャンセルアクションの追加
        if(isHaveCancel){
            let cancelAction:UIAlertAction = UIAlertAction(title: "キャンセル",
                style: UIAlertActionStyle.Cancel,
                handler:{
                    (action:UIAlertAction) -> Void in
                    self.delegate.alertClose("キャンセル")
            })
            
            alert.addAction(cancelAction)
        }
        
        //defaultアクションの追加
        for defaultTitle in defaultActionArray{
            let defaultAction:UIAlertAction = UIAlertAction(title: defaultTitle,
                style: UIAlertActionStyle.Default,
                handler:{
                    (action:UIAlertAction) -> Void in
                    self.delegate.alertClose(defaultTitle)
            })
            alert.addAction(defaultAction)
        }
        
        alert.view.alpha = 0.7
        
        view.presentViewController(alert, animated: true, completion: nil)
    }
}