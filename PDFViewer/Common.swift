//
//  Common.swift
//
//  Created by 大工智博 on 2015/02/16.
//  Copyright (c) 2015年 marunoPharmacy. All rights reserved.
//

import Foundation
import UIKit

let NUMBER_LENGTH = 11
let FORMATTER = NSDateFormatter()
let DATE_FORMAT = "yyyy/MM/dd"

let SIZE_LARGE = "large"
let SIZE_SMALL = "small"
let SIZE_FULL = "full"
let IMAGE_PATH:String =  NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] 


let MENU_BTN_RADIUS:CGFloat = 20
let MENU_BTN_BORDER:CGFloat = 3
let BTN_BORDER:CGFloat = 1
let REQUIRED_BACKRGB:UInt = 0xFF0000
let REQUIRED_BACK_ALPHA:CGFloat = 0.1

let POP_WIDTH:CGFloat = 200
let POP_BUTTON_HEIGHT:CGFloat = 80

enum MultiJobKbn:Int{
    case None = 0
    case Facility = 1
    case Hospital = 2
    case CareManager = 3
    case Other = 99
}

enum EditMode{
    case Show
    case Add
    case Edit
    case Select
}

enum ReportSendKbn:Int{
    case Fax = 0
    case Mail = 1
    case FaxMail = 2
    case None = 4
}

//アプリケーション情報
struct Application{
    
    static var currentView:UIView? = nil
    static var currentViewController:UIViewController? = nil
    static var isShowProgress = false
}

//
struct Hard{
    //ユーザ情報登録省略フラグ
    //static var isPatientRegistIgnore = false
    
    //struct Store{
        //static var id:Int! = nil
    //}
}


//共通メソッド
class Common{
    class func getDefaultValueFromOptional(key:String?,defaultVal:String) -> String{
        if let returnVal = key{
            return returnVal
        }
        return defaultVal
    }
    class func getDefaultValueFromOptional(key:Int?,defaultVal:Int) -> Int{
        if let returnVal = key{
            return returnVal
        }
        return defaultVal
    }
    class func getDefaultValueFromOptional(key:NSDate?,defaultVal:NSDate) -> NSDate{
        if let returnVal = key{
            return returnVal
        }
        return defaultVal
    }
    
    class func setBtnBorder(btn:UIButton,cornerRadius:CGFloat=MENU_BTN_RADIUS,borderColor:CGColor=UIColor.blueColor().CGColor,borderWidth:CGFloat=BTN_BORDER){
        btn.layer.cornerRadius = cornerRadius
        btn.layer.borderColor = borderColor
        btn.layer.borderWidth = borderWidth
    }
    
    class func changeDisplayEditMode(txtArray_lockTextFileds: [UITextField]!,enabled:Bool){
        if(txtArray_lockTextFileds == nil){return}
        for item in txtArray_lockTextFileds{
            item.enabled = enabled
        }
    }
    
    class func changeRequiredBackgroundcolor(txtArray_Required: [UITextField]!,isSetColor:Bool){
        if(txtArray_Required == nil){return}
        for item in txtArray_Required{
            if(isSetColor){
                item.backgroundColor = getUIColorFromRGB(REQUIRED_BACKRGB)
            }else{
                item.backgroundColor = nil
            }
        }
    }
    
    class func getUIColorFromRGB(rgb: UInt,alpha:CGFloat=REQUIRED_BACK_ALPHA) -> UIColor {
        return UIColor(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
    
    /**
    必須項目のチェックを行う
    */
    class func checkInput(txtArray_Required: [UITextField]!) -> Bool{
        if txtArray_Required == nil {return true}
        for item in txtArray_Required{
            if let text = item.text{
                if text == ""{return false}
            }else{
                return false
            }
        }
        return true
    }
    
    
    class func createNaviBarPopover(naviBarAddBtnArray:[UIButton]) -> UIPopoverController{
        let contentViewController = UIViewController()
        contentViewController.preferredContentSize = CGSizeMake(POP_WIDTH, POP_BUTTON_HEIGHT * CGFloat(naviBarAddBtnArray.count))
        contentViewController.view.backgroundColor = UIColor.whiteColor()
        
        for (index,item) in naviBarAddBtnArray.enumerate(){
            if(index != 0){
                let separator: UIView = UIView(frame: CGRectMake(10, POP_BUTTON_HEIGHT * CGFloat(index), POP_WIDTH - 20, 1))
                separator.backgroundColor = UIColor.blueColor()
                contentViewController.view.addSubview(separator)
            }
            item.frame = CGRectMake(0, POP_BUTTON_HEIGHT * CGFloat(index), POP_WIDTH, POP_BUTTON_HEIGHT)
            contentViewController.view.addSubview(item)
        }
        
        
        return UIPopoverController(contentViewController: contentViewController)
    }
    
    /**
    
    */
    class func setSegmentedMultiJob(seg_MultiJobKbn:UISegmentedControl,kbn:Int){
        switch kbn{
        case 99:
            seg_MultiJobKbn.selectedSegmentIndex = 4
        default:
            seg_MultiJobKbn.selectedSegmentIndex = kbn
        }
    }
    
    class func getMultiJobKbnFromSegmented(seg_MultiJobKbn:UISegmentedControl)->Int{
        switch seg_MultiJobKbn.selectedSegmentIndex{
        case MultiJobKbn.Other.rawValue:
            return 4
        default:
            return seg_MultiJobKbn.selectedSegmentIndex
        }
        
    }
    
    class func getEnumMultiJobKbnFromSegmented(seg_MultiJobKbn:UISegmentedControl)->MultiJobKbn{
        switch seg_MultiJobKbn.selectedSegmentIndex{
        case 0:
            return MultiJobKbn.None
        case 1:
            return MultiJobKbn.Facility
        case 2:
            return MultiJobKbn.Hospital
        case 3:
            return MultiJobKbn.CareManager
        default:
            return MultiJobKbn.Other
        }
        
    }
    
    
    class func getOld(birthday:NSDate)->Int{
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Year, fromDate: birthday, toDate: NSDate(), options: [])
        if(components.year > 200){return 0}
        return components.year
    }
    
    
    class func getDateFromEraString(eraName:String,year:String,month:String,day:String) -> NSDate{
        var adYear = Int(year)!
        switch eraName{
        case "大正":
            adYear += 1911
        case "昭和":
            adYear += 1925
        case "平成":
            adYear += 1988
        default:
            return NSDate()
        }
        return Common.getStrToDate(adYear.description + "/" + month + "/" + day)!
    }
    
    class func isEmpty(object:AnyObject?) -> Bool{
        if let strObject = object as? String {
            if(strObject == ""){return true}
        }
        if(object == nil){return true}
        return false
    }
    
    class func isExist(object:AnyObject?) -> Bool{
        if let strObject = object as? String {
            if(strObject == ""){return false}
        }
        if(object == nil){return false}
        return true
    }
    
    class func setBirthdayFromDate(birthday:NSDate,txt_EraName:UITextField,txt_BirthdayYear:UITextField,txt_BirthdayMonth:UITextField,txt_BirthdayDay:UITextField){
        if(Common.getOld(birthday) == 0){return}
        
        let calendar = NSCalendar.currentCalendar()
        
        let comps:NSDateComponents = calendar.components([NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day],fromDate: birthday)
        
        var eraYear = 0
        if(comps.year > 1988){
            txt_EraName.text = "平成"
            eraYear = comps.year - 1988
        }else if(comps.year > 1925){
            txt_EraName.text = "昭和"
            eraYear = comps.year - 1925
        }else{
            txt_EraName.text = "大正"
            eraYear = comps.year - 1911
        }
        
        txt_BirthdayYear.text = eraYear.description
        txt_BirthdayMonth.text = comps.month.description
        txt_BirthdayDay.text = comps.day.description
        
    }
    
    class func getDateToStr(date:NSDate,format:String = DATE_FORMAT) -> String{
        FORMATTER.dateFormat = format;
        FORMATTER.locale = NSLocale(localeIdentifier: "ja_JP")
        return FORMATTER.stringFromDate(date);
    }
    
    class func getStrToDate(date:String) -> NSDate?{
        FORMATTER.dateFormat = DATE_FORMAT;
        FORMATTER.locale = NSLocale(localeIdentifier: "ja_JP")
        let formatDate = FORMATTER.dateFromString(date)
        
        return formatDate
    }
    
    
    class func getSecondView(storyboardNm:String,viewIdentifier:String) -> UIViewController{
        let storyboard = UIStoryboard(name: storyboardNm, bundle: nil)
        let secondView:UIViewController = storyboard.instantiateViewControllerWithIdentifier(viewIdentifier) 
        secondView.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        return secondView
    }
    
    class func getSeparateTel(strNum:String)->String{
        if(strNum.characters.count < NUMBER_LENGTH){return strNum}
        let separetedStr:NSMutableString = NSMutableString(string: strNum)
        separetedStr.insertString("-", atIndex: 7)
        separetedStr.insertString("-", atIndex: 3)
        return separetedStr as String
    }
    
    /*
    永続記憶領域にアプリデータをシリアライズ
    */
    class func setNSUserDefaults(){
        // 保存データを全削除
        let appDomain:String = NSBundle.mainBundle().bundleIdentifier!
        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
    }
    
    /*
    永続記憶領域からアプリデータをデシリアライズ
    */
    class func getNSUserDefaults(){
        //ユーザ登録省略フラグ
        //Hard.isPatientRegistIgnore = NSUserDefaults.standardUserDefaults().boolForKey("isPatientRegistIgnore")
    }
    
    /*
    永続記憶領域からアプリデータを削除
    */
    class func deleteUserDefaults(){
        // 保存データを全削除
        let appDomain:String = NSBundle.mainBundle().bundleIdentifier!
        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
    }
    
    
    
    class func moveView(storyboardNm:String,storyboardID:String,view:UIViewController){
        self.moveView(storyboardNm, storyboardID: storyboardID, view: view,animated:true)

    }
    
    class func moveView(storyboardNm:String,storyboardID:String,view:UIViewController, animated: Bool){
        let storyboard = UIStoryboard(name: storyboardNm, bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(storyboardID) 
        view.presentViewController(vc, animated: animated, completion: nil)
            }
}
